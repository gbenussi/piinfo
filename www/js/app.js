var app;

app = angular.module('starter', ['ionic', 'ngCordova', 'ui.router', 'ionic-datepicker', 'restangular', 'leaflet-directive']);

app.run(function($ionicPlatform, $rootScope, $state, UserData) {
  $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState) {
    if (!UserData.type && toState.name !== 'login') {
      console.log('DENY : Redirecting to Login');
      $state.go('login');
      event.preventDefault();
    } else {
      console.log('ALLOW');
    }
  });
  return $ionicPlatform.ready(function() {
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});

app.config(function(RestangularProvider) {
  RestangularProvider.setBaseUrl('http://ec2-52-36-44-90.us-west-2.compute.amazonaws.com:3000/api');
});
