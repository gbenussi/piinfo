app = angular.module('starter', ['ionic', 'ngCordova', 'ui.router', 'ionic-datepicker', 'restangular', 'leaflet-directive'])

app.run ($ionicPlatform, $rootScope, $state, UserData) ->
  $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState) ->
    if !UserData.type and toState.name != 'login'
        console.log 'DENY : Redirecting to Login'
        $state.go('login');
        event.preventDefault()
    else
        console.log 'ALLOW'
    return
  $ionicPlatform.ready ->
    if window.cordova and window.cordova.plugins.Keyboard
      # Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      # for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar true
      # Don't remove this line unless you know what you are doing. It stops the viewport
      # from snapping when text inputs are focused. Ionic handles this internally for
      # a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll true
    if window.StatusBar
      StatusBar.styleDefault()
    return

app.config (RestangularProvider) ->
  # RestangularProvider.setBaseUrl('http://ec2-52-36-32-60.us-west-2.compute.amazonaws.com:3000/api');
  # RestangularProvider.setBaseUrl 'http://158.170.255.174:3000/api'

  # RestangularProvider.setBaseUrl 'http://192.168.1.41:3000/api'
  RestangularProvider.setBaseUrl 'http://ec2-52-36-44-90.us-west-2.compute.amazonaws.com:3000/api'

  # RestangularProvider.setBaseUrl 'https://nlohzlxwgp.localtunnel.me/api'
  # RestangularProvider.setBaseUrl 'http://localhost:3000/api'
  # RestangularProvider.setBaseUrl 'https://gugree.herokuapp.com/api'
  return
