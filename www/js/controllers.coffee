app = angular.module('starter')

app.factory('GugreeCommons', ($ionicLoading) ->
    return {
        showIonicLoading: (text) ->
            $ionicLoading.show({
                template: text
            })
            return
        , hideIonicLoading: () ->
            $ionicLoading.hide()
            return
    }
)

app.controller 'RootController', ['$scope', 'Restangular', '$cordovaGeolocation', '$ionicSideMenuDelegate', '$ionicModal', 'UserData', ($scope, Restangular, $cordovaGeolocation, $ionicSideMenuDelegate, $ionicModal, UserData) ->
    $scope.userType = _.capitalize(UserData.type)

    $scope.esPersona = () ->
        return UserData.type == 'PERSONA'

    $scope.esRecolector = () ->
        return UserData.type == 'RECOLECTOR'

    $scope.toggleLeft = () ->
        $ionicSideMenuDelegate.toggleLeft();
        return
    $scope.toggleRight = () ->
        $ionicSideMenuDelegate.toggleRight();
        return
    return
]

app.factory('Desechos', ['Restangular', (Restangular) ->
    Desechos = Restangular.all('desechos')
    return Desechos
])

app.controller 'VerController', ['$scope', 'Restangular', '$cordovaGeolocation', '$log', 'Desechos', ($scope, Restangular, $cordovaGeolocation, $log, Desechos) ->
    $scope.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.4488900, lng: -70.6692650},
      scrollwheel: false,
      zoom: 10
    });

    directionsPanel = document.getElementById("my_textual_div");
    myLatlng = new google.maps.LatLng(-33.4488900, -70.6692650)
    # // Create the tsp object
    tsp = new BpTspSolver($scope.map, directionsPanel);

    # // Set your preferences
    tsp.setAvoidHighways(true);
    tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);
    # tsp.setTravelMode(google.maps.DirectionsTravelMode.BICYCLING);

    Desechos.getList().then (desechos) ->
        puntos = []
        _(desechos).forEach (value, index) ->
            myLatlng = new google.maps.LatLng(value.lat, value.lon);
            # marker = new google.maps.Marker({
            #     position: myLatlng,
            #     title:"Hello World!",
            #     icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+index+'|FE6256|000000'
            # });
            # marker.setMap($scope.map);
            puntos.push({
                lat: _.toNumber(value.lat),
                lng: _.toNumber(value.lon),
            })
            cityCircle = new google.maps.Circle({
              strokeColor: '#FF0000',
              strokeOpacity: 0.8,
              strokeWeight: 2,
              fillColor: '#FF0000',
              fillOpacity: 0.35,
              map: $scope.map,
              center: myLatlng,
              radius: Math.sqrt(value.cantidad * 200)
            });
            tsp.addWaypoint(myLatlng);
            return
        # tsp.addAddress("san adolfo 2915 puente alto");
        onSolveCallback = () ->
            path = []
            order = tsp.getOrder();
            dir = tsp.getGDirections()
            directionsDisplay = new google.maps.DirectionsRenderer({
                suppressMarkers : true
            });
            directionsDisplay.setMap($scope.map);
            directionsDisplay.setDirections(dir);
            i = 1
            _(order).forEach( (index) ->
                path.push(puntos[ index ])
                if i < order.length
                    myOptions = {
                        content: _.toString(i++),
                        boxStyle: {
                            # background: '#000',
                            # border: "1px solid black",
                            textAlign: "center",
                            fontSize: "14pt",
                            color: "white",
                            "text-shadow": "-2px 0 black, 0 2px black, 2px 0 black, 0 -2px black";
                            # width: "90px",
                            "z-index": "99"
                        },
                        disableAutoPan: true,
                        # pixelOffset: new google.maps.Size(-45, 0),
                        position: myLatlng = new google.maps.LatLng(puntos[ index ]),
                        closeBoxURL: "",
                        isHidden: false,
                        enableEventPropagation: true
                    };

                    label = new InfoBox(myOptions);
                    label.open($scope.map);

                directionsDisplay.setPanel(document.getElementById('right-menu'));
                document.getElementById("right-menu").style.marginTop = "44px";
                document.getElementById("right-menu").style['overflow-y'] = "scroll";
                return
            )
            # rutaDeRecoleccion = new google.maps.Polyline({
            #     path: path,
            #     geodesic: true,
            #     strokeColor: '#FF0000',
            #     strokeOpacity: 1.0,
            #     strokeWeight: 2
            # });
            # rutaDeRecoleccion.setMap($scope.map);
            return

        # // Solve the problem (start and end up at the first location)
        tsp.solveRoundTrip(onSolveCallback);


        # // Retrieve the solution (so you can display it to the user or do whatever :-)
        dir = tsp.getGDirections();#  // This is a normal GDirections object.
        order = tsp.getOrder();
        durations = tsp.getDurations();
        return

    segundos_timeout_geolocalizacion = 20
    console.warn "Utilizando " + segundos_timeout_geolocalizacion + " segundos de timeout para obtener la ubicación del usuario"

    $cordovaGeolocation.getCurrentPosition({
        timeout: 1000 * segundos_timeout_geolocalizacion,
        enableHighAccuracy: false
    }).then ((position) ->
      lat = position.coords.latitude
      long = position.coords.longitude
      $scope.map.setCenter(new google.maps.LatLng( lat, long ) );
      $scope.map.setZoom( 15 )
      console.log 'Coordenadas cargadas correctamente'
      return
    ), (err) ->
        alert("ha ocurrido un error al cargar su posicion :'(")
        console.log("Error cargando su posición:")
        console.log err
        return
    , { timeout: 1 }
    return
]

app.factory('BpTspSolver', () ->
    return {
        BpTspSolver: (map, directionsPanel) ->
            return new BpTspSolver(map, directionsPanel);
    }
)

app.directive('file', () ->
  return {
    restrict: 'AE',
    scope: {
      file: '@'
    },
    link: (scope, el, attrs) ->
      el.bind('change', (event) ->
        files = event.target.files;
        file = files[0];
        scope.file = file;
        scope.$parent.file = file;
        scope.$apply();
      );
      return
  };
);

app.controller 'VerPuntosController', ['$scope', 'Restangular', '$cordovaGeolocation', '$log', 'Desechos', 'BpTspSolver', '$ionicPopup', ($scope, Restangular, $cordovaGeolocation, $log, Desechos, BpTspSolver, $ionicPopup) ->
    $scope.formulario = {
      tipos: [
        'papel',
        'carton',
        'metal',
      ],
      tipos_activos: [
         { nombre: 'Papel', activo: true },
         { nombre: 'Cartón', activo: true },
         { nombre: 'Metal', activo: true },
      ],
      cantidad: 1,
      lat: null,
      lon: null
    }

    # $scope.creds = {
    #     bucket: 'your_bucket',
    #     access_key: 'your_access_key',
    #     secret_key: 'your_secret_key'
    # }
    #
    # $scope.upload = () ->
    #     AWS.config.update({ accessKeyId: $scope.creds.access_key, secretAccessKey: $scope.creds.secret_key });
    #     AWS.config.region = 'us-east-1';
    #     bucket = new AWS.S3({ params: { Bucket: $scope.creds.bucket } });
    #
    #     if $scope.file
    #         params = { Key: $scope.file.name, ContentType: $scope.file.type, Body: $scope.file, ServerSideEncryption: 'AES256' };
    #
    #         bucket.putObject(params, (err, data) ->
    #           if err
    #               alert(err.message);
    #               return false;
    #           else
    #               alert('Upload Done');
    #           return
    #         )
    #         .on('httpUploadProgress', (progress) ->
    #             console.log(Math.round(progress.loaded / progress.total * 100) + '% done');
    #             return
    #         );
    #     else
    #         alert('No File Selected');
    #     return

    $scope.seleccionar_area_active = false
    $scope.seleccionar_area_text   = "Seleccionar Área"

    directionsDisplay = new google.maps.DirectionsRenderer({
        suppressMarkers : true
    });

    $scope.map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: -33.4488900, lng: -70.6692650},
      scrollwheel: false,
      zoom: 6
    });

    directionsPanel = document.getElementById("my_textual_div");
    myLatlng = new google.maps.LatLng(-33.4488900, -70.6692650)

    markers = [];
    all_markers = []

    $scope.calculando_ruta = false
    $scope.boton_calcular_ruta = 'Calcular Ruta'

    $scope.calcularRuta = () ->
        getShortestRouteFromMarkers(markers, $scope.map)
        return

    Desechos.getList().then (desechos) ->
        puntos = []
        _(desechos).forEach (value, index) ->
            myLatlng = new google.maps.LatLng(value.lat, value.lon);
            marker = new google.maps.Marker({
                map: $scope.map,
                position: myLatlng,
                title:"Hello World!",
                icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+index+'|FE6256|000000',
                id: index,
                selected: false,
            });
            all_markers.push(marker)
            google.maps.event.addListener(marker, 'mouseover', (event) ->
                if this.selected
                    this.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + this.id + '|FE6256|000000');
                    _.pull(markers, marker)
                    this.selected = false
                    directionsDisplay.setMap(null);
                    # getShortestRouteFromMarkers(markers, $scope.map)
                else
                    this.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + this.id + '|4CAF50|000000');
                    markers.push(marker);
                    this.selected = true
                    directionsDisplay.setMap(null);
                    # getShortestRouteFromMarkers(markers, $scope.map)

            );

            puntos.push({
                lat: _.toNumber(value.lat),
                lng: _.toNumber(value.lon),
            })
            return
        return

    segundos_timeout_geolocalizacion = 20
    console.warn "Utilizando " + segundos_timeout_geolocalizacion + " segundos de timeout para obtener la ubicación del usuario"

    worldCoords = [
        new google.maps.LatLng(-85.1054596961173, -180),
        new google.maps.LatLng(85.1054596961173, -180),
        new google.maps.LatLng(85.1054596961173, 180),
        new google.maps.LatLng(-85.1054596961173, 180),
        new google.maps.LatLng(-85.1054596961173, 0)
    ]

    lat_limit  = -33.4488900
    long_limit = -70.6692650

    radius = 0.03

    LimitCoords = [
        new google.maps.LatLng(lat_limit + radius, long_limit + radius),
        new google.maps.LatLng(lat_limit + radius, long_limit - radius),
        new google.maps.LatLng(lat_limit - radius, long_limit - radius),
        new google.maps.LatLng(lat_limit - radius, long_limit + radius)
    ]

    poly = new google.maps.Polygon({
        paths: [worldCoords, LimitCoords],
        strokeColor: '#000000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#000000',
        fillOpacity: 0.35
    });

    poly.setMap($scope.map);

    $scope.map.setCenter(new google.maps.LatLng( lat_limit, long_limit ) );

    $scope.rectangle = null

    coordenadasCargadas = (lat = null, long = null) ->
        #   CARGAR RECTANGULO
        radius = 0.005

        rectangleOptions = {
            editable: true,
            draggable: true,
            fillColor: '#F44336',
            strokeColor: '#F44336',
        }

        if lat != null
            rectangleOptions.bounds = {
                north: lat + radius,
                south: lat - radius,
                east: long + radius,
                west: long - radius
            };

        $scope.rectangle = new google.maps.Rectangle(rectangleOptions);

        showNewRect = (event) ->
            ne = $scope.rectangle.getBounds().getNorthEast();
            sw = $scope.rectangle.getBounds().getSouthWest();

            _(all_markers).forEach((marker) ->
                if sw.lat() <= marker.position.lat() <= ne.lat() and sw.lng() <= marker.position.lng() <= ne.lng()
                    marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|4CAF50|000000');
                    marker.selected = true
                    directionsDisplay.setMap(null);
                    markers.push(marker);
                else
                    marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|FE6256|000000');
                    marker.selected = false
                    directionsDisplay.setMap(null);
                    _.pull(markers, marker)
              #   google.maps.event.trigger(marker, 'mouseover');
                return
            )

            contentString = '<b>Rectangle moved.</b><br>' +
            'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' +
            'New south-west corner: ' + sw.lat() + ', ' + sw.lng();
          #   infoWindow.setContent(contentString);
          #   infoWindow.setPosition(ne);
          #   infoWindow.open($scope.map);
            infoWindow = new google.maps.InfoWindow();
            return
        $scope.rectangle.addListener('bounds_changed', showNewRect);
        #   // CARGAR RECTANGULO

        $scope.seleccionarArea = () ->
            $scope.seleccionar_area_text = if $scope.seleccionar_area_active then "Seleccionar Área" else 'Cancelar Selección'
            $scope.seleccionar_area_active = !$scope.seleccionar_area_active
          #   $scope.seleccionar_area_text = "Seleccionar Área"
            if $scope.seleccionar_area_active
                center = $scope.map.getCenter();
                map_bounds = $scope.map.getBounds()
                variacion = 0.003
                bounds = {}
                if (map_bounds.R.j - 1 * variacion) - (map_bounds.R.R + 1 * variacion) > 0 and ((map_bounds.j.R - 1 * variacion) - (map_bounds.j.j + 1 * variacion)) > 0
                    bounds = {
                        north: map_bounds.R.j - 1 * variacion,
                        south: map_bounds.R.R + 1 * variacion,
                        east: map_bounds.j.R - 1 * variacion,
                        west: map_bounds.j.j + 1 * variacion,
                    }
                else
                    bounds = {
                        north: map_bounds.R.j - 0.0001,
                        south: map_bounds.R.R + 1 * 0.0001,
                        east: map_bounds.j.R - 1 * 0.0001,
                        west: map_bounds.j.j + 1 * 0.0001,
                    }

                console.log ((map_bounds.R.j - 1 * variacion) - (map_bounds.R.R + 1 * variacion))
                console.log ((map_bounds.j.R - 1 * variacion) - (map_bounds.j.j + 1 * variacion))
                console.log bounds
                # lat = center.lat()
                # long = center.lng()
                # bounds = {
                #     north: lat + radius,
                #     south: lat - radius,
                #     east: long + radius,
                #     west: long - radius
                # };
                $scope.rectangle.setBounds(bounds);
                showNewRect()
                $scope.rectangle.setMap($scope.map);
            else
                $scope.rectangle.setMap(null);
                _(all_markers).forEach((marker) ->
                    marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|FE6256|000000');
                    marker.selected = false
                    directionsDisplay.setMap(null);
                    _.pull(markers, marker)
                    return
                )
            return
        return

    $scope.map.setZoom( 12 )
    coordenadasCargadas(lat_limit, long_limit)

    rutaCalculadaCorrectamente = () ->
        $scope.calculando_ruta = false
        $scope.boton_calcular_ruta = 'Calcular Ruta'
        $scope.seleccionar_area_text = "Seleccionar Área"
        $scope.seleccionar_area_active = false
        $scope.rectangle.setMap(null);
        return

    # coordenadasCargadas(lat, long)

    # $cordovaGeolocation.getCurrentPosition({
    #     timeout: 1000 * segundos_timeout_geolocalizacion,
    #     enableHighAccuracy: false
    # }).then ((position) ->
    #   lat = position.coords.latitude
    #   long = position.coords.longitude
    #   $scope.map.setCenter(new google.maps.LatLng( lat, long ) );
    #   console.log 'Coordenadas cargadas correctamente'
    #   coordenadasCargadas(lat, long)
    #   return
    # ), (err) ->
    #     console.log("Error cargando su posición:")
    #     console.log err
    #     coordenadasCargadas()
    #     return
    # , { timeout: 1 }

    $scope.showAlert = (title, template) ->
        alertPopup = $ionicPopup.alert({
            title: title,
            template: template
        });

        alertPopup.then((res) ->
            console.log('Thank you for not eating my delicious ice cream cone')
            return
        )
        return

    getShortestRouteFromMarkers = (markers, map) ->
        if markers.length < 2
            $scope.showAlert("Error", "Debe seleccionar al menos 2 puntos para poder calcular una ruta")
            return
        $scope.calculando_ruta = true
        $scope.boton_calcular_ruta = 'Calculando Ruta'
        directionsDisplay.setMap(null);
        # // Create the tsp object
        # tsp = BpTspSolver.BpTspSolver($scope.map, directionsPanel)
        tsp = BpTspSolver.BpTspSolver($scope.map, directionsPanel)
        tsp.startOver()

        # // Set your preferences
        tsp.setAvoidHighways(true);
        tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);
        _(markers).forEach((marker) ->
            # console.log "A"
            # console.log marker
            # console.log marker.position.lat()
            tsp.addWaypoint(new google.maps.LatLng( marker.position.lat(), marker.position.lng() ));
            return
        )
        # console.log tsp.getWaypoints()
        onSolveCallback = () ->
            path = []
            order = tsp.getOrder();
            dir = tsp.getGDirections()
            directionsDisplay.setMap(map);
            directionsDisplay.setDirections(dir);
            i = 1
            _(order).forEach( (index) ->
                # path.push(puntos[ index ])
                # console.log puntos[ index ]
                if i < order.length
                    myOptions = {
                        content: _.toString(i++),
                        boxStyle: {
                            # background: '#000',
                            # border: "1px solid black",
                            textAlign: "center",
                            fontSize: "14pt",
                            color: "white",
                            "text-shadow": "-2px 0 black, 0 2px black, 2px 0 black, 0 -2px black";
                            # width: "90px",
                            "z-index": "99"
                        },
                        disableAutoPan: true,
                        # pixelOffset: new google.maps.Size(-45, 0),
                        position: markers.position,
                        closeBoxURL: "",
                        isHidden: false,
                        enableEventPropagation: true
                    };

                    label = new InfoBox(myOptions);
                    label.open($scope.map);
                directionsDisplay.setPanel(null);
                directionsDisplay.setPanel(document.getElementById('right-menu'));
                document.getElementById("right-menu").style.marginTop = "44px";
                document.getElementById("right-menu").style['overflow-y'] = "scroll";
                return
            )
            return

        # // Solve the problem (start and end up at the first location)
        tsp.solveRoundTrip(onSolveCallback);
        rutaCalculadaCorrectamente()
        return

    return
]

app.controller 'DesechosController', ['$scope', 'Restangular', '$cordovaGeolocation', '$ionicSideMenuDelegate', '$state', 'Desechos', '$ionicPopup', ($scope, Restangular, $cordovaGeolocation, $ionicSideMenuDelegate, $state, Desechos, $ionicPopup) ->
  $scope.boton_enviar_val = 'Cargando ubicación...'
  $scope.formulario = {
    tipos: [
      'papel',
      'carton',
      'metal',
    ],
    cantidad: 1,
    lat: null,
    lon: null
  }
  $scope.formulario.tipo = $scope.formulario.tipos[0]

  segundos_timeout_geolocalizacion = 10

  $cordovaGeolocation.getCurrentPosition({
    timeout: segundos_timeout_geolocalizacion * 1000,
    enableHighAccuracy: false
  }).then ((position) ->
    lat = position.coords.latitude
    long = position.coords.longitude
    $scope.formulario.lat = lat
    $scope.formulario.lon = long
    $scope.boton_enviar_val = 'Enviar'
    console.log 'Coordenadas cargadas correctamente'
    return
  ), (err) ->
    $scope.boton_enviar_val = 'No ha sido posible cargar su ubicación :('
    console.log err
    $scope.showAlert("Error", "No ha sido posible obtener su ubicación. Pruebe activando la geolocalización de su dispositivo.")
    return
  $scope.showAlert = (title, template) ->
      alertPopup = $ionicPopup.alert({
          title: title,
          template: template
      });

      alertPopup.then((res) ->
          console.log('Thank you for not eating my delicious ice cream cone')
          return
      )
      return
  disabledDates = []

  datePickerCallback = (val) ->
    console.log val
    if typeof val == 'undefined'
      console.log 'No date selected'
    else
      console.log 'Selected date is : ', val
    # this.val(val)
    return

  today = new Date
  menos_un_dia = new Date
  menos_un_dia.setDate (new Date).getDate() - 1
  datepickerObject =
    titleLabel: 'Indique su disponibilidad'
    todayLabel: 'Hoy'
    closeLabel: 'Cerrar'
    setLabel: 'Listo'
    setButtonType: 'button-assertive'
    todayButtonType: 'button-assertive'
    closeButtonType: 'button-assertive'
    inputDate: today
    mondayFirst: true
    disabledDates: disabledDates
    weekDaysList: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb']
    monthList: [ 'Ene', 'Feb', 'Marzo', 'Abril', 'May', 'Junio', 'Julio', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic']
    templateType: 'popup'
    showTodayButton: 'true'
    modalHeaderColor: 'bar-positive'
    modalFooterColor: 'bar-positive'
    from: menos_un_dia
    dateFormat: 'dd/MM/yyyy'
    closeOnSelect: false
  # $scope.datepickerObjectInicio = datepickerObject
  # $scope.datepickerObjectFin = datepickerObject
  $scope.datepickerObjectInicio = _.clone(datepickerObject)
  $scope.datepickerObjectFin = _.clone(datepickerObject)
  $scope.datepickerObjectFin.inputDate = new Date
  $scope.datepickerObjectFin.inputDate.setDate (new Date).getDate() + 7

  $scope.datepickerObjectInicio.callback = (val) ->
    datePickerCallback val
    $scope.datepickerObjectInicio.inputDate.setDate new Date(val).getDate()
    return

  $scope.datepickerObjectFin.callback = (val) ->
    datePickerCallback val
    $scope.datepickerObjectFin.inputDate.setDate new Date(val).getDate()
    return

  $scope.enviarFormulario = ->
    nuevo_desecho = {
        tipo: $scope.formulario.tipo,
        cantidad: $scope.formulario.cantidad,
        lat: $scope.formulario.lat,
        lon: $scope.formulario.lon
    }
    console.log(nuevo_desecho)
    Desechos.post(nuevo_desecho).then () ->
        $state.go('root.desecho_publicado_correctamente')
        # $scope.openModal()
    return

  return
]

app.factory('Tips', () ->
    tips = []
    # for i in [1..10]
    #     tips.push({
    #         id: i,
    #         title: chance.sentence({words: 3}),
    #         image: "http://lorempixel.com/400/200/sports/" + i,
    #         content: chance.paragraph(),
    #     })
    tips = [
        {
            id: 0,
            title: "Elige bien lo que compras",
            content: "Esto es muy fácil. Cuando vayas al supermercado, fíjate que tengan las etiquetas que apoyan el reciclaje de basura.",
        },
        {
            id: 1,
            title: "Vacía los envases",
            content: "Luego de utilizarlos, vacía todos los envases y aplástalos para que ocupen menos espacio. De esta forma podrás acumular más residuos y se reducirá el gasto en transporte.",
        },
        {
            id: 2,
            title: "Un contenedor para cada producto",
            content: "Cada uno de los contenedores sirve para un material específico. Debes fijarte en esto, pues en las plantas de clasificación deberán devolverlos a los correspondientes. De esta forma, ahorras gasto de tiempo y contaminación.",
        },
        {
            id: 3,
            title: "Reutiliza",
            content: "Muchos de los objetos que tiramos a la basura sirven para otras funciones luego de ser utilizados. Para aminorar la cantidad de residuos, trata de encontrar nuevas formas de uso. Así ahorrarás incluso dinero.",
        },
        {
            id: 4,
            title: "Haz compost",
            content: "Una excelente forma de reutilizar la basura orgánica, como cáscaras de frutas y verduras, es crear compost, que sirve como un increíble abono para tus plantas y pasto.",
        },
    ]
    return tips
)

app.controller('TipsController', ['$scope', 'Tips', 'GugreeCommons', ($scope, Tips, GugreeCommons) ->
    # GugreeCommons.showIonicLoading();
    $scope.tips = Tips
    return
])

app.controller('TipController', ['$scope', 'Tips', '$stateParams', '$ionicScrollDelegate', ($scope, Tips, $stateParams, $ionicScrollDelegate) ->
    $ionicScrollDelegate.scrollTop();
    $scope.tip = _.find(Tips, { id: _.toInteger($stateParams.tipId) })
    return
])

app.service('UserData', () ->
    user = this
    user.type = null
)

app.controller('LoginController', ['$scope', 'UserData', '$state', ($scope, UserData, $state) ->
    UserData.type = null
    $scope.loginAsPerson = () ->
        UserData.type = 'PERSONA'
        console.log UserData.type
        $state.go('root.home');
        return
    $scope.loginAsRecolector = () ->
        UserData.type = 'RECOLECTOR'
        console.log UserData.type
        $state.go('root.home');
        return
    return
])
