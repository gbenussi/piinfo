var app;

app = angular.module('starter');

app.factory('GugreeCommons', function($ionicLoading) {
  return {
    showIonicLoading: function(text) {
      $ionicLoading.show({
        template: text
      });
    },
    hideIonicLoading: function() {
      $ionicLoading.hide();
    }
  };
});

app.controller('RootController', [
  '$scope', 'Restangular', '$cordovaGeolocation', '$ionicSideMenuDelegate', '$ionicModal', 'UserData', function($scope, Restangular, $cordovaGeolocation, $ionicSideMenuDelegate, $ionicModal, UserData) {
    $scope.userType = _.capitalize(UserData.type);
    $scope.esPersona = function() {
      return UserData.type === 'PERSONA';
    };
    $scope.esRecolector = function() {
      return UserData.type === 'RECOLECTOR';
    };
    $scope.toggleLeft = function() {
      $ionicSideMenuDelegate.toggleLeft();
    };
    $scope.toggleRight = function() {
      $ionicSideMenuDelegate.toggleRight();
    };
  }
]);

app.factory('Desechos', [
  'Restangular', function(Restangular) {
    var Desechos;
    Desechos = Restangular.all('desechos');
    return Desechos;
  }
]);

app.controller('VerController', [
  '$scope', 'Restangular', '$cordovaGeolocation', '$log', 'Desechos', function($scope, Restangular, $cordovaGeolocation, $log, Desechos) {
    var directionsPanel, myLatlng, segundos_timeout_geolocalizacion, tsp;
    $scope.map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: -33.4488900,
        lng: -70.6692650
      },
      scrollwheel: false,
      zoom: 10
    });
    directionsPanel = document.getElementById("my_textual_div");
    myLatlng = new google.maps.LatLng(-33.4488900, -70.6692650);
    tsp = new BpTspSolver($scope.map, directionsPanel);
    tsp.setAvoidHighways(true);
    tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);
    Desechos.getList().then(function(desechos) {
      var dir, durations, onSolveCallback, order, puntos;
      puntos = [];
      _(desechos).forEach(function(value, index) {
        var cityCircle;
        myLatlng = new google.maps.LatLng(value.lat, value.lon);
        puntos.push({
          lat: _.toNumber(value.lat),
          lng: _.toNumber(value.lon)
        });
        cityCircle = new google.maps.Circle({
          strokeColor: '#FF0000',
          strokeOpacity: 0.8,
          strokeWeight: 2,
          fillColor: '#FF0000',
          fillOpacity: 0.35,
          map: $scope.map,
          center: myLatlng,
          radius: Math.sqrt(value.cantidad * 200)
        });
        tsp.addWaypoint(myLatlng);
      });
      onSolveCallback = function() {
        var dir, directionsDisplay, i, order, path;
        path = [];
        order = tsp.getOrder();
        dir = tsp.getGDirections();
        directionsDisplay = new google.maps.DirectionsRenderer({
          suppressMarkers: true
        });
        directionsDisplay.setMap($scope.map);
        directionsDisplay.setDirections(dir);
        i = 1;
        _(order).forEach(function(index) {
          var label, myOptions;
          path.push(puntos[index]);
          if (i < order.length) {
            myOptions = {
              content: _.toString(i++),
              boxStyle: {
                textAlign: "center",
                fontSize: "14pt",
                color: "white",
                "text-shadow": "-2px 0 black, 0 2px black, 2px 0 black, 0 -2px black",
                "z-index": "99"
              },
              disableAutoPan: true,
              position: myLatlng = new google.maps.LatLng(puntos[index]),
              closeBoxURL: "",
              isHidden: false,
              enableEventPropagation: true
            };
            label = new InfoBox(myOptions);
            label.open($scope.map);
          }
          directionsDisplay.setPanel(document.getElementById('right-menu'));
          document.getElementById("right-menu").style.marginTop = "44px";
          document.getElementById("right-menu").style['overflow-y'] = "scroll";
        });
      };
      tsp.solveRoundTrip(onSolveCallback);
      dir = tsp.getGDirections();
      order = tsp.getOrder();
      durations = tsp.getDurations();
    });
    segundos_timeout_geolocalizacion = 20;
    console.warn("Utilizando " + segundos_timeout_geolocalizacion + " segundos de timeout para obtener la ubicación del usuario");
    $cordovaGeolocation.getCurrentPosition({
      timeout: 1000 * segundos_timeout_geolocalizacion,
      enableHighAccuracy: false
    }).then((function(position) {
      var lat, long;
      lat = position.coords.latitude;
      long = position.coords.longitude;
      $scope.map.setCenter(new google.maps.LatLng(lat, long));
      $scope.map.setZoom(15);
      console.log('Coordenadas cargadas correctamente');
    }), function(err) {
      alert("ha ocurrido un error al cargar su posicion :'(");
      console.log("Error cargando su posición:");
      console.log(err);
    }, {
      timeout: 1
    });
  }
]);

app.factory('BpTspSolver', function() {
  return {
    BpTspSolver: function(map, directionsPanel) {
      return new BpTspSolver(map, directionsPanel);
    }
  };
});

app.directive('file', function() {
  return {
    restrict: 'AE',
    scope: {
      file: '@'
    },
    link: function(scope, el, attrs) {
      el.bind('change', function(event) {
        var file, files;
        files = event.target.files;
        file = files[0];
        scope.file = file;
        scope.$parent.file = file;
        return scope.$apply();
      });
    }
  };
});

app.controller('VerPuntosController', [
  '$scope', 'Restangular', '$cordovaGeolocation', '$log', 'Desechos', 'BpTspSolver', '$ionicPopup', function($scope, Restangular, $cordovaGeolocation, $log, Desechos, BpTspSolver, $ionicPopup) {
    var LimitCoords, all_markers, coordenadasCargadas, directionsDisplay, directionsPanel, getShortestRouteFromMarkers, lat_limit, long_limit, markers, myLatlng, poly, radius, rutaCalculadaCorrectamente, segundos_timeout_geolocalizacion, worldCoords;
    $scope.formulario = {
      tipos: ['papel', 'carton', 'metal'],
      tipos_activos: [
        {
          nombre: 'Papel',
          activo: true
        }, {
          nombre: 'Cartón',
          activo: true
        }, {
          nombre: 'Metal',
          activo: true
        }
      ],
      cantidad: 1,
      lat: null,
      lon: null
    };
    $scope.seleccionar_area_active = false;
    $scope.seleccionar_area_text = "Seleccionar Área";
    directionsDisplay = new google.maps.DirectionsRenderer({
      suppressMarkers: true
    });
    $scope.map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: -33.4488900,
        lng: -70.6692650
      },
      scrollwheel: false,
      zoom: 6
    });
    directionsPanel = document.getElementById("my_textual_div");
    myLatlng = new google.maps.LatLng(-33.4488900, -70.6692650);
    markers = [];
    all_markers = [];
    $scope.calculando_ruta = false;
    $scope.boton_calcular_ruta = 'Calcular Ruta';
    $scope.calcularRuta = function() {
      getShortestRouteFromMarkers(markers, $scope.map);
    };
    Desechos.getList().then(function(desechos) {
      var puntos;
      puntos = [];
      _(desechos).forEach(function(value, index) {
        var marker;
        myLatlng = new google.maps.LatLng(value.lat, value.lon);
        marker = new google.maps.Marker({
          map: $scope.map,
          position: myLatlng,
          title: "Hello World!",
          icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + index + '|FE6256|000000',
          id: index,
          selected: false
        });
        all_markers.push(marker);
        google.maps.event.addListener(marker, 'mouseover', function(event) {
          if (this.selected) {
            this.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + this.id + '|FE6256|000000');
            _.pull(markers, marker);
            this.selected = false;
            return directionsDisplay.setMap(null);
          } else {
            this.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + this.id + '|4CAF50|000000');
            markers.push(marker);
            this.selected = true;
            return directionsDisplay.setMap(null);
          }
        });
        puntos.push({
          lat: _.toNumber(value.lat),
          lng: _.toNumber(value.lon)
        });
      });
    });
    segundos_timeout_geolocalizacion = 20;
    console.warn("Utilizando " + segundos_timeout_geolocalizacion + " segundos de timeout para obtener la ubicación del usuario");
    worldCoords = [new google.maps.LatLng(-85.1054596961173, -180), new google.maps.LatLng(85.1054596961173, -180), new google.maps.LatLng(85.1054596961173, 180), new google.maps.LatLng(-85.1054596961173, 180), new google.maps.LatLng(-85.1054596961173, 0)];
    lat_limit = -33.4488900;
    long_limit = -70.6692650;
    radius = 0.03;
    LimitCoords = [new google.maps.LatLng(lat_limit + radius, long_limit + radius), new google.maps.LatLng(lat_limit + radius, long_limit - radius), new google.maps.LatLng(lat_limit - radius, long_limit - radius), new google.maps.LatLng(lat_limit - radius, long_limit + radius)];
    poly = new google.maps.Polygon({
      paths: [worldCoords, LimitCoords],
      strokeColor: '#000000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#000000',
      fillOpacity: 0.35
    });
    poly.setMap($scope.map);
    $scope.map.setCenter(new google.maps.LatLng(lat_limit, long_limit));
    $scope.rectangle = null;
    coordenadasCargadas = function(lat, long) {
      var rectangleOptions, showNewRect;
      if (lat == null) {
        lat = null;
      }
      if (long == null) {
        long = null;
      }
      radius = 0.005;
      rectangleOptions = {
        editable: true,
        draggable: true,
        fillColor: '#F44336',
        strokeColor: '#F44336'
      };
      if (lat !== null) {
        rectangleOptions.bounds = {
          north: lat + radius,
          south: lat - radius,
          east: long + radius,
          west: long - radius
        };
      }
      $scope.rectangle = new google.maps.Rectangle(rectangleOptions);
      showNewRect = function(event) {
        var contentString, infoWindow, ne, sw;
        ne = $scope.rectangle.getBounds().getNorthEast();
        sw = $scope.rectangle.getBounds().getSouthWest();
        _(all_markers).forEach(function(marker) {
          var ref, ref1;
          if ((sw.lat() <= (ref = marker.position.lat()) && ref <= ne.lat()) && (sw.lng() <= (ref1 = marker.position.lng()) && ref1 <= ne.lng())) {
            marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|4CAF50|000000');
            marker.selected = true;
            directionsDisplay.setMap(null);
            markers.push(marker);
          } else {
            marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|FE6256|000000');
            marker.selected = false;
            directionsDisplay.setMap(null);
            _.pull(markers, marker);
          }
        });
        contentString = '<b>Rectangle moved.</b><br>' + 'New north-east corner: ' + ne.lat() + ', ' + ne.lng() + '<br>' + 'New south-west corner: ' + sw.lat() + ', ' + sw.lng();
        infoWindow = new google.maps.InfoWindow();
      };
      $scope.rectangle.addListener('bounds_changed', showNewRect);
      $scope.seleccionarArea = function() {
        var bounds, center, map_bounds, variacion;
        $scope.seleccionar_area_text = $scope.seleccionar_area_active ? "Seleccionar Área" : 'Cancelar Selección';
        $scope.seleccionar_area_active = !$scope.seleccionar_area_active;
        if ($scope.seleccionar_area_active) {
          center = $scope.map.getCenter();
          map_bounds = $scope.map.getBounds();
          variacion = 0.003;
          bounds = {};
          if ((map_bounds.R.j - 1 * variacion) - (map_bounds.R.R + 1 * variacion) > 0 && ((map_bounds.j.R - 1 * variacion) - (map_bounds.j.j + 1 * variacion)) > 0) {
            bounds = {
              north: map_bounds.R.j - 1 * variacion,
              south: map_bounds.R.R + 1 * variacion,
              east: map_bounds.j.R - 1 * variacion,
              west: map_bounds.j.j + 1 * variacion
            };
          } else {
            bounds = {
              north: map_bounds.R.j - 0.0001,
              south: map_bounds.R.R + 1 * 0.0001,
              east: map_bounds.j.R - 1 * 0.0001,
              west: map_bounds.j.j + 1 * 0.0001
            };
          }
          console.log((map_bounds.R.j - 1 * variacion) - (map_bounds.R.R + 1 * variacion));
          console.log((map_bounds.j.R - 1 * variacion) - (map_bounds.j.j + 1 * variacion));
          console.log(bounds);
          $scope.rectangle.setBounds(bounds);
          showNewRect();
          $scope.rectangle.setMap($scope.map);
        } else {
          $scope.rectangle.setMap(null);
          _(all_markers).forEach(function(marker) {
            marker.setIcon('http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=' + marker.id + '|FE6256|000000');
            marker.selected = false;
            directionsDisplay.setMap(null);
            _.pull(markers, marker);
          });
        }
      };
    };
    $scope.map.setZoom(12);
    coordenadasCargadas(lat_limit, long_limit);
    rutaCalculadaCorrectamente = function() {
      $scope.calculando_ruta = false;
      $scope.boton_calcular_ruta = 'Calcular Ruta';
      $scope.seleccionar_area_text = "Seleccionar Área";
      $scope.seleccionar_area_active = false;
      $scope.rectangle.setMap(null);
    };
    $scope.showAlert = function(title, template) {
      var alertPopup;
      alertPopup = $ionicPopup.alert({
        title: title,
        template: template
      });
      alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
      });
    };
    getShortestRouteFromMarkers = function(markers, map) {
      var onSolveCallback, tsp;
      if (markers.length < 2) {
        $scope.showAlert("Error", "Debe seleccionar al menos 2 puntos para poder calcular una ruta");
        return;
      }
      $scope.calculando_ruta = true;
      $scope.boton_calcular_ruta = 'Calculando Ruta';
      directionsDisplay.setMap(null);
      tsp = BpTspSolver.BpTspSolver($scope.map, directionsPanel);
      tsp.startOver();
      tsp.setAvoidHighways(true);
      tsp.setTravelMode(google.maps.DirectionsTravelMode.WALKING);
      _(markers).forEach(function(marker) {
        tsp.addWaypoint(new google.maps.LatLng(marker.position.lat(), marker.position.lng()));
      });
      onSolveCallback = function() {
        var dir, i, order, path;
        path = [];
        order = tsp.getOrder();
        dir = tsp.getGDirections();
        directionsDisplay.setMap(map);
        directionsDisplay.setDirections(dir);
        i = 1;
        _(order).forEach(function(index) {
          var label, myOptions;
          if (i < order.length) {
            myOptions = {
              content: _.toString(i++),
              boxStyle: {
                textAlign: "center",
                fontSize: "14pt",
                color: "white",
                "text-shadow": "-2px 0 black, 0 2px black, 2px 0 black, 0 -2px black",
                "z-index": "99"
              },
              disableAutoPan: true,
              position: markers.position,
              closeBoxURL: "",
              isHidden: false,
              enableEventPropagation: true
            };
            label = new InfoBox(myOptions);
            label.open($scope.map);
          }
          directionsDisplay.setPanel(null);
          directionsDisplay.setPanel(document.getElementById('right-menu'));
          document.getElementById("right-menu").style.marginTop = "44px";
          document.getElementById("right-menu").style['overflow-y'] = "scroll";
        });
      };
      tsp.solveRoundTrip(onSolveCallback);
      rutaCalculadaCorrectamente();
    };
  }
]);

app.controller('DesechosController', [
  '$scope', 'Restangular', '$cordovaGeolocation', '$ionicSideMenuDelegate', '$state', 'Desechos', '$ionicPopup', function($scope, Restangular, $cordovaGeolocation, $ionicSideMenuDelegate, $state, Desechos, $ionicPopup) {
    var datePickerCallback, datepickerObject, disabledDates, menos_un_dia, segundos_timeout_geolocalizacion, today;
    $scope.boton_enviar_val = 'Cargando ubicación...';
    $scope.formulario = {
      tipos: ['papel', 'carton', 'metal'],
      cantidad: 1,
      lat: null,
      lon: null
    };
    $scope.formulario.tipo = $scope.formulario.tipos[0];
    segundos_timeout_geolocalizacion = 10;
    $cordovaGeolocation.getCurrentPosition({
      timeout: segundos_timeout_geolocalizacion * 1000,
      enableHighAccuracy: false
    }).then((function(position) {
      var lat, long;
      lat = position.coords.latitude;
      long = position.coords.longitude;
      $scope.formulario.lat = lat;
      $scope.formulario.lon = long;
      $scope.boton_enviar_val = 'Enviar';
      console.log('Coordenadas cargadas correctamente');
    }), function(err) {
      $scope.boton_enviar_val = 'No ha sido posible cargar su ubicación :(';
      console.log(err);
      $scope.showAlert("Error", "No ha sido posible obtener su ubicación. Pruebe activando la geolocalización de su dispositivo.");
    });
    $scope.showAlert = function(title, template) {
      var alertPopup;
      alertPopup = $ionicPopup.alert({
        title: title,
        template: template
      });
      alertPopup.then(function(res) {
        console.log('Thank you for not eating my delicious ice cream cone');
      });
    };
    disabledDates = [];
    datePickerCallback = function(val) {
      console.log(val);
      if (typeof val === 'undefined') {
        console.log('No date selected');
      } else {
        console.log('Selected date is : ', val);
      }
    };
    today = new Date;
    menos_un_dia = new Date;
    menos_un_dia.setDate((new Date).getDate() - 1);
    datepickerObject = {
      titleLabel: 'Indique su disponibilidad',
      todayLabel: 'Hoy',
      closeLabel: 'Cerrar',
      setLabel: 'Listo',
      setButtonType: 'button-assertive',
      todayButtonType: 'button-assertive',
      closeButtonType: 'button-assertive',
      inputDate: today,
      mondayFirst: true,
      disabledDates: disabledDates,
      weekDaysList: ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vie', 'Sáb'],
      monthList: ['Ene', 'Feb', 'Marzo', 'Abril', 'May', 'Junio', 'Julio', 'Ago', 'Sept', 'Oct', 'Nov', 'Dic'],
      templateType: 'popup',
      showTodayButton: 'true',
      modalHeaderColor: 'bar-positive',
      modalFooterColor: 'bar-positive',
      from: menos_un_dia,
      dateFormat: 'dd/MM/yyyy',
      closeOnSelect: false
    };
    $scope.datepickerObjectInicio = _.clone(datepickerObject);
    $scope.datepickerObjectFin = _.clone(datepickerObject);
    $scope.datepickerObjectFin.inputDate = new Date;
    $scope.datepickerObjectFin.inputDate.setDate((new Date).getDate() + 7);
    $scope.datepickerObjectInicio.callback = function(val) {
      datePickerCallback(val);
      $scope.datepickerObjectInicio.inputDate.setDate(new Date(val).getDate());
    };
    $scope.datepickerObjectFin.callback = function(val) {
      datePickerCallback(val);
      $scope.datepickerObjectFin.inputDate.setDate(new Date(val).getDate());
    };
    $scope.enviarFormulario = function() {
      var nuevo_desecho;
      nuevo_desecho = {
        tipo: $scope.formulario.tipo,
        cantidad: $scope.formulario.cantidad,
        lat: $scope.formulario.lat,
        lon: $scope.formulario.lon
      };
      console.log(nuevo_desecho);
      Desechos.post(nuevo_desecho).then(function() {
        return $state.go('root.desecho_publicado_correctamente');
      });
    };
  }
]);

app.factory('Tips', function() {
  var tips;
  tips = [];
  tips = [
    {
      id: 0,
      title: "Elige bien lo que compras",
      content: "Esto es muy fácil. Cuando vayas al supermercado, fíjate que tengan las etiquetas que apoyan el reciclaje de basura."
    }, {
      id: 1,
      title: "Vacía los envases",
      content: "Luego de utilizarlos, vacía todos los envases y aplástalos para que ocupen menos espacio. De esta forma podrás acumular más residuos y se reducirá el gasto en transporte."
    }, {
      id: 2,
      title: "Un contenedor para cada producto",
      content: "Cada uno de los contenedores sirve para un material específico. Debes fijarte en esto, pues en las plantas de clasificación deberán devolverlos a los correspondientes. De esta forma, ahorras gasto de tiempo y contaminación."
    }, {
      id: 3,
      title: "Reutiliza",
      content: "Muchos de los objetos que tiramos a la basura sirven para otras funciones luego de ser utilizados. Para aminorar la cantidad de residuos, trata de encontrar nuevas formas de uso. Así ahorrarás incluso dinero."
    }, {
      id: 4,
      title: "Haz compost",
      content: "Una excelente forma de reutilizar la basura orgánica, como cáscaras de frutas y verduras, es crear compost, que sirve como un increíble abono para tus plantas y pasto."
    }
  ];
  return tips;
});

app.controller('TipsController', [
  '$scope', 'Tips', 'GugreeCommons', function($scope, Tips, GugreeCommons) {
    $scope.tips = Tips;
  }
]);

app.controller('TipController', [
  '$scope', 'Tips', '$stateParams', '$ionicScrollDelegate', function($scope, Tips, $stateParams, $ionicScrollDelegate) {
    $ionicScrollDelegate.scrollTop();
    $scope.tip = _.find(Tips, {
      id: _.toInteger($stateParams.tipId)
    });
  }
]);

app.service('UserData', function() {
  var user;
  user = this;
  return user.type = null;
});

app.controller('LoginController', [
  '$scope', 'UserData', '$state', function($scope, UserData, $state) {
    UserData.type = null;
    $scope.loginAsPerson = function() {
      UserData.type = 'PERSONA';
      console.log(UserData.type);
      $state.go('root.home');
    };
    $scope.loginAsRecolector = function() {
      UserData.type = 'RECOLECTOR';
      console.log(UserData.type);
      $state.go('root.home');
    };
  }
]);
