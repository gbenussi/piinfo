app.config(function($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/login");
  $stateProvider.state('root', {
    templateUrl: "partials/root.html"
  }).state('login', {
    url: "/login",
    templateUrl: "partials/login.html",
    controller: "LoginController"
  }).state('root.home', {
    url: "/",
    templateUrl: "partials/home.html"
  }).state('root.tip', {
    url: "/tips/:tipId",
    templateUrl: "partials/tip.html",
    controller: "TipController"
  }).state('root.tips', {
    url: "/tips",
    templateUrl: "partials/tips.html",
    controller: "TipsController"
  }).state('root.desecho_publicado_correctamente', {
    templateUrl: "partials/desecho_publicado_correctamente.html"
  }).state('root.publicar', {
    url: "/publicar",
    templateUrl: "partials/publicar.html"
  }).state('root.ver', {
    url: "/ver",
    templateUrl: "partials/ver.html",
    controller: 'VerController'
  }).state('root.ver_puntos', {
    url: "/verPuntos",
    templateUrl: "partials/ver_puntos.html",
    controller: 'VerPuntosController'
  }).state('root.state1', {
    url: "/state1",
    templateUrl: "partials/state1.html"
  }).state('root.state1.list', {
    url: "/list",
    templateUrl: "partials/state1.list.html",
    controller: function($scope) {
      return $scope.items = ["A", "List", "Of", "Items"];
    }
  }).state('root.state2', {
    url: "/state2",
    templateUrl: "partials/state2.html"
  }).state('root.state2.list', {
    url: "/list",
    templateUrl: "partials/state2.list.html",
    controller: function($scope) {
      return $scope.things = ["A", "Set", "Of", "Things"];
    }
  });
});
