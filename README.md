Gugree
=====================

Gugree es una aplicación que facilita el reciclaje, creando un nexo entre generadores de desechos reciclables y recolectores de estos.

## Cómo ejecutar

### Web
Para ejecutar la aplicación en un navegador web, simplemente abrir la terminal, posicionarse en la carpeta base y escribir el siguiente comando:

```sh
ionic serve
```

### Android

Para construir la aplicación en dispositivos Android, se deben ejecutar los siguientes comandos en el directorio base del proyecto:

#### Pruebas

Si se desea probar la aplicación, ejecutar los siguientes comandos:
```sh
ionic build android
ionic run android
```

#### Publicar

Para publicar la aplicación, ejecutar los siguientes comandos

```sh
# Construimos el release de Gugree
cordova build --release android
# Generamos una llave privada (keytool es parte del JDK)
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
# Firmamos el apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore ./platforms/android/build/outputs/apk/android-release-unsigned.apk alias_name
# Optimizamos el APK
zipalign -v 4 ./platforms/android/build/outputs/apk/android-release-unsigned.apk gugree.apk
```

Para mas detalles de este paso, visitar la siguiente url: http://ionicframework.com/docs/guide/publishing.html
