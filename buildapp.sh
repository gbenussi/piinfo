echo "cordova build --release android..."
cordova build --release android
# genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
echo "jarsigner..."
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore ./platforms/android/build/outputs/apk/android-release-unsigned.apk alias_name
echo "Eliminando gugree.apk"
rm ./gugree.apk
echo "zipalign..."
zipalign -v 4 ./platforms/android/build/outputs/apk/android-release-unsigned.apk gugree.apk
